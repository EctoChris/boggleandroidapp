package com.example.chrisford.boggleproject;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chrisford on 6/6/18.
 */

public class CustomAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> guessedWords;

    public CustomAdapter (Context context, ArrayList<String> guessedWords)
    {
        super(context, R.layout.customlistlayout, guessedWords);
        this.guessedWords = guessedWords;
    }

    public void updateList(String guess)
    {
        guessedWords.add(guess);
    }

@NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // return super.getView(position, convertView, parent);

        //To build views from xml layout files (aka not programatically)
        //we need to use the layout inflater
        LayoutInflater layoutInflater = LayoutInflater.from(getContext()); //use existing, don't build your own
        //Inflate method takes an xml layout file and builds it into java class objects of type view
        //other two args are: ViewGroup to Add to(Add automatically to existing group of views)
        //                    True/False add to this group now
        View view = layoutInflater.inflate(R.layout.customlistlayout, parent, false);

        //Our row is made up of a LinearLayout and holds textViews and another LinearLayout
        //all UI elements (including LinearLayout) inherit from class View
        //so this inflate thing just builds all the objects and returns them as a view reference
        //we can cast back if we need to e.g LinearLayout layout = (LinearLayout)view;

        //References to layout
        TextView wordLocation = (TextView)view.findViewById(R.id.wordSpot);
        TextView pointsLocation = (TextView)view.findViewById(R.id.pointsSpot);

        wordLocation.setText(guessedWords.get(position));
        int length = guessedWords.get(position).length();
        int points;
        switch(length)
        {
            case 3:
                points = 1;
                pointsLocation.setText("+ " + points + " points");
                break;
            case 4:
                points = 2;
                pointsLocation.setText("+ " + points + " points");
                break;
            case 5:
                points = 3;
                pointsLocation.setText("+ " + points + " points");
                break;
            case 6:
                points = 4;
                pointsLocation.setText("+ " + points + " points");
            case 7:
                points = 5;
                pointsLocation.setText("+ " + points + " points");
            case 8:
                points = 6;
                pointsLocation.setText("+ " + points + " points");
            default:

        }

        //once done, return the built view
        return view;
    }


}
