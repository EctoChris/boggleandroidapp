package com.example.chrisford.boggleproject;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bolts.CancellationTokenSource;
import bolts.Task;
import it.sephiroth.android.library.uigestures.UIGestureRecognizer;
import it.sephiroth.android.library.uigestures.UIGestureRecognizerDelegate;
import it.sephiroth.android.library.uigestures.UIPanGestureRecognizer;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class MainPage extends Activity
             implements  UIGestureRecognizer.OnActionListener, UIGestureRecognizerDelegate.Callback {

    //Initialize first CancellationToken for cancelling previously coloured list
    CancellationTokenSource cts = new CancellationTokenSource();

    //Declaration of database class
    Database db;

    //Declaration of UI Elements
    TextView statusBar;
    LinearLayout mainll;
    TextView guessingBar;
    TextView timerLabel;
    TextView listTitle;
    TextView score;
    TextView bottomScore;
    ListView solutionsList;
    ImageView backButton;
    Button solveGameButton;
    ListAdapter listAdapter;
    ArrayAdapter<String> adapter;

    //Empty String used to hold user word guess attempts
    String selectedLettersList = "";
    String[][] letterMatrix = new String[4][4];
    long startTime = 0;

    //List of Strings to hold all previous correctly guessed words
    ArrayList<String> guessedWords;

    CustomAdapter customAdapter;

    //Score variable
    int gameScore = 0;

    //Variables used in calculating screen width for programatically responsive design
    int screenWidth;
    int screenHeight;

    //Declaration of LetterUtil class for useful 'letter' related methods
    LetterUtil LU;

    //List of all boggle buttons
    List<Button> BoggleButtons;

    //Variables for potential use in algorithm to find all possible solutions for present boggle grid
    ArrayList<Integer> usedButtons;
    Map BoggleMap = new HashMap();

    //List to ensure users cannot guess words using the same letter twice
    List<Button> usedButtonsB;
    final UIGestureRecognizerDelegate delegate = new UIGestureRecognizerDelegate(null);

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage);

        //Initialization of Lists
        usedButtons = new ArrayList<>();
        BoggleButtons = new ArrayList<>();
        usedButtonsB = new ArrayList<>();
        guessedWords = new ArrayList<>();
        guessedWords = new ArrayList<String>();

        //When reading txt files from 'assets', they must be accessed via an activity. The MainPage Activity will send it's context to Database Class for reading ability
        db = new Database(this.getApplicationContext());

        //Finding UI Elements Id
        score = findViewById(R.id.score);
        bottomScore = findViewById(R.id.bottomTotalScoreLabel);
        backButton = findViewById(R.id.backButton);
        guessingBar = findViewById(R.id.guessingBar);
        solveGameButton = findViewById(R.id.solveGameButton);
        listTitle = findViewById(R.id.listTitle);
        timerLabel = findViewById(R.id.timerLabel);


        getScreenDimensions();

        //Initialization of LetterUtil class for useful 'letter' related methods
        LU = new LetterUtil(db, screenWidth);

        createStatusBar();
        UIPanGestureRecognizer recognizer = new UIPanGestureRecognizer(this);
        delegate.addGestureRecognizer(recognizer);
        createBoggleGrid();
        createList();

        //Back Button Functionality (Setting up Click Listener)
        View.OnClickListener clickListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (v.equals(backButton)) {
                    //Go to next screen using intent
                    Intent nextScreen = new Intent(v.getContext(), TitlePage.class);
                    startActivity(nextScreen);
                }
                else if (v.equals(solveGameButton)) {
                    solveGameClicked(v);
                }
            }
        };

        //Set the click listener
        backButton.setOnClickListener(clickListener);
        solveGameButton.setOnClickListener(clickListener);



    }

    @Override
    public boolean shouldBegin(UIGestureRecognizer uiGestureRecognizer) {
        return false;
    }

    @Override
    public boolean shouldRecognizeSimultaneouslyWithGestureRecognizer(UIGestureRecognizer uiGestureRecognizer, UIGestureRecognizer uiGestureRecognizer1) {
        return false;
    }

    @Override
    public boolean shouldReceiveTouch(UIGestureRecognizer uiGestureRecognizer) {
        return false;
    }

    //toastThread had to be created to ensure toast would display at the beginning of the 'solveGame' function.
    public void toastThread()
    {
        //For each letter in the letterMatrix (find all possible solutions that start with that letter)
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                LU.startFindingCombinations(letterMatrix, i, j);
            }
        }

        //Remove user-guessed words and repopulate with words found from algorithm
        guessedWords.clear();
        int possibleScore = 0;
        for(String str : LU.returnPossibleSolutions())
        {
            guessedWords.add(0, str);
            customAdapter.notifyDataSetChanged();

            //Calculate appropriate scores for each word found from algorithm
            switch(str.length())
            {
                case 3:
                    possibleScore += 1;
                    break;
                case 4:
                    possibleScore +=2;
                    break;
                case 5:
                    possibleScore += 3;
                    break;
                case 6:
                    possibleScore += 4;
                    break;
                case 7:
                    possibleScore += 5;
                    break;
                default:
                    break;
            }
        }

        //Change relevant UI to newly found words
        score.setText(Integer.toString(possibleScore));
        listTitle.setText("Solutions");
        gameScore = possibleScore;
        bottomScore.setText(Integer.toString(gameScore));
    }

    public void solveGameClicked(View view) {
        final Context newContext = this;

        Toast.makeText(newContext, "Finding Solutions.... Please wait", Toast.LENGTH_LONG).show();
        //New threads had to be created to make sure Toast would display properly.
        (new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastThread();
                        }
                    });
                } catch (Exception error) {
                    System.out.println(error.getMessage());
                }
            }
        }).start();

    }


    // Touch Listener for View and Buttons
    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View v, MotionEvent motionEvent)
        {
            //MotionEvent for First Button Click (Beginning of selection of new word)
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
            {
                //If Colouring previous word still in progress, cancel and return all buttons to default colours
                if (LU.getColouringInProgress())
                {
                    for(Button b : BoggleButtons)
                    {
                        b.setBackgroundResource(R.drawable.boggle_button);
                    }
                    cts.cancel();
                    cts = new CancellationTokenSource();
                }

                //Change UI of first selected button and add guessed word to selectedLettersList
                try
                {
                    Button b = (Button) v;
                    usedButtonsB.add(b);
                    selectedLettersList += b.getText();
                    guessingBar.setText(selectedLettersList);
                    if (screenWidth > 1100)
                        b.setBackgroundResource(R.drawable.boggle_button_selected_tablet);
                    else
                        b.setBackgroundResource(R.drawable.boggle_button_selected);
                    usedButtons.add(b.getId());

                } catch(ClassCastException cex)
                {
                    System.out.println(cex.getMessage());
                }
            }

            //Loop through all buttons
            for(Button b : BoggleButtons)
            {
                //Get current button location
                int locations[] = new int[2];
                b.getLocationOnScreen(locations);

                //Check if button location corresponds with touch
                if (inViewInBounds(b, (int) motionEvent.getRawX(), (int) motionEvent.getRawY()))
                {
                    //First Check if first button has been selected
                    if (usedButtons.size() != 0)
                    {
                        //Colour first button if colour has been cancelled by a cancelled token
                        if (screenWidth < 1100)
                             usedButtonsB.get(0).setBackgroundResource(R.drawable.boggle_button_selected);
                        else
                             usedButtonsB.get(0).setBackgroundResource(R.drawable.boggle_button_selected_tablet);

                        //If newly selected button is next to last selected button, colour and add to selectedLettersList
                        if (LU.isWithinRangeOfPreviouslyClicked(b.getId(), usedButtons.get(usedButtons.size() - 1), usedButtons))
                        {
                            if (screenWidth > 1100)
                                b.setBackgroundResource(R.drawable.boggle_button_selected_tablet);
                            else
                                b.setBackgroundResource(R.drawable.boggle_button_selected);
                            usedButtons.add(b.getId());
                            usedButtonsB.add(b);
                            selectedLettersList += b.getText();
                            guessingBar.setText(selectedLettersList);
                        }
                    }
                }
            }

            //Touch Release
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && selectedLettersList.length() > 0)
            {
                //Create new CancellationTokenSource to send into 'LU.changeListColour'
                cts = new CancellationTokenSource();



                //Verify if word exists in database
                if (db.verifyWord(selectedLettersList) && selectedLettersList.length() > 2)
                {
                    //If user has already guessed -> colour letters Grey
                    if (guessedWords.contains(selectedLettersList))
                    {
                        LU.changeListColour(cts.getToken(), usedButtonsB, 1);
                    }

                    else {

                        //Set statusbar message to visible
                        statusBar.setVisibility(VISIBLE);

                        //Colour Selection in green to notify user of result
                        LU.changeListColour(cts.getToken(), usedButtonsB, 0);

                        //Find length of guessed word to add score and display appropriate message tp user
                        switch(selectedLettersList.length())
                        {
                            case 3:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 1 points for 3 Letter Word");
                                gameScore += 1;
                                break;
                            case 4:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 2 points for 4 Letter Word");
                                gameScore += 2;
                                break;
                            case 5:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 3 points for 5 Letter Word");
                                gameScore += 3;
                                break;
                            case 6:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 4 points for 6 Letter Word");
                                gameScore += 4;
                                break;
                            case 7:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 5 points for 7 Letter Word");
                                gameScore += 5;
                                break;
                            case 8:
                                LU.changeStatusBar(cts.getToken(), statusBar, 0, "+ 6 points for 8 Letter Word");
                                gameScore += 6;
                                break;
                            default:
                                break;

                        }
                        //Add word to list of already guessed words
                        guessedWords.add(0, selectedLettersList);
                        customAdapter.notifyDataSetChanged();
                    }

                }
                //If word does not exist in database, change selected letters to red and output error message
                else {
                    LU.changeListColour(cts.getToken(), usedButtonsB, 2);
                }

                //After guessing a word, create new list of 'usedButtons' and selectedLettersList, ready for next user guess
                usedButtons = new ArrayList<>();
                usedButtons.clear();
                usedButtonsB = new ArrayList<>();
                selectedLettersList = "";

                //Reset Guessing Bar(user entered letters)
                guessingBar.setText(selectedLettersList);

                //Adjust TextView 'score' to updated gameScore
                score.setText(String.valueOf(gameScore));
                bottomScore.setText(String.valueOf(gameScore));
            }
            return delegate.onTouchEvent(v, motionEvent);
        }


    }

    //Method to see if touch on view is the coordinate of the inner section of a button
    private boolean inViewInBounds(View view, int x, int y) {
        Rect outRect = new Rect();
        int[] location = new int[2];
        view.getDrawingRect(outRect);
        view.getLocationOnScreen(location);
        outRect.offset(location[0], location[1]);

        //Creation of smaller Rect within outRect to make diagonal joining of letters easier
        Rect innerRect = new Rect();
        innerRect.set((int)(outRect.left + (outRect.width() * 0.2)), (int)(outRect.top + (outRect.height() * 0.2)), (int)(outRect.right - (outRect.width() * 0.2)), (int)(outRect.bottom - (outRect.height() * 0.2)) );
        return innerRect.contains(x, y);
    }


    // ui gesture recognizer event callback
    @Override
    public void onGestureRecognized(@NonNull final UIGestureRecognizer recognizer) {
        Toast.makeText(getApplicationContext(), "This should never be called", Toast.LENGTH_LONG).show();
    }

    //Method used for responsive UI calculations
    public void getScreenDimensions()
    {
        screenWidth  =   Resources.getSystem().getDisplayMetrics().widthPixels;
        screenHeight =   Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public void createLetters()
    {
        //Creating 2 dimensional string array and assigning randomized letters from 'LetterUtil' class.
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                letterMatrix[i][j] = LU.getRandomLetter();
            }
        }
        int counter = 0;

        //Set UIButton titles
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                BoggleButtons.get(counter).setText(letterMatrix[i][j]);
                counter++;
            }
        }
    }


    public void createBoggleGrid()
    {
        //Id variables used in loop to create appropriate button id's programmatically
        int idrow = 0;
        int idcolumn;

        //Reference Main Linear Layout
        mainll = (LinearLayout)findViewById(R.id.mainPageLinearLayout);

        //Set on touch listener
        mainll.setOnTouchListener(new MyTouchListener());

        //Dynamically adding boggle buttons to LinearLayout in xml
        for(int j = 0; j < 4 ; j++)
        {
            LinearLayout row = new LinearLayout(this);
            LinearLayout.LayoutParams rowLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            row.setBackgroundResource(R.drawable.boggle_board_background);

            int index;
            if (screenWidth > 400)
                index = 4 + j;
            else
                index = 3 + j;

            mainll.addView(row, index, rowLayoutParams);
            mainll.setHorizontalGravity(Gravity.CENTER);
            idcolumn=0;
            for (int i = 0; i < 4; i++) {
                Button newButton = new Button(this);
                newButton.setMinimumWidth(0);
                newButton.setBackgroundResource(R.drawable.boggle_button);
                if (screenWidth > 1100)
                    newButton.setTextSize(35);
                else
                    newButton.setTextSize(24);
                if (screenWidth > 400) {
                    newButton.setWidth((int) (screenWidth * 0.15));
                    newButton.setHeight((int) (screenWidth * 0.15));
                }
                else {
                    newButton.setWidth((int)(screenWidth * 0.15));
                    newButton.setHeight((int)(screenWidth * 0.05));
                }
                newButton.setOnTouchListener(new MyTouchListener());
                LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                if (screenWidth > 400 && screenWidth < 1000)
                     buttonLayoutParams.setMargins(5, 5, 5, 5);
                else if (screenWidth > 1300)
                {
                    buttonLayoutParams.setMargins(15, 15, 15, 15);
                    newButton.setTextSize(45);
                }
                else if (screenWidth < 1300 && screenWidth > 1100) {
                    buttonLayoutParams.setMargins(10, 10, 10, 10);
                    newButton.setTextSize(40);
                }
                else if (screenWidth < 1100 && screenWidth > 900)
                    buttonLayoutParams.setMargins(10,10,10,10);
                else {
                    buttonLayoutParams.setMargins(3, 3, 3, 3);
                }
                newButton.setLayoutParams(buttonLayoutParams);
                // Generate id for current button:
                String tempId = Integer.toString(idrow) + Integer.toString(idcolumn);
                int newId = Integer.parseInt(tempId);
                newButton.setId(newId);
                BoggleMap.put(newId, newButton);

                row.addView(newButton);
                BoggleButtons.add(newButton);
                row.setHorizontalGravity(Gravity.CENTER);
                idcolumn++;
            }
            idrow++;
        }
        createLetters();
    }

    public void createStatusBar()
    {
        //Find LinearLayout to input statusBar
        LinearLayout topNavBar = (LinearLayout)findViewById(R.id.topNavBar);

        //Create Instance of statusBar
        statusBar = new TextView(this);

        //Set Attributes of statusBar
        statusBar.setText("Status Bar");
        statusBar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        statusBar.setHeight((int) (screenWidth * 0.3));

        if (screenWidth < 1100) {
            statusBar.setWidth((int) (screenWidth * 0.6));
            statusBar.setTextSize(15);
        }
        else if (screenWidth > 1200)
        {
            statusBar.setWidth((int)(screenWidth * 0.72));
            statusBar.setTextSize(30);
        }
        else {
            statusBar.setWidth((int)(screenWidth * 0.65));
            statusBar.setTextSize(24);
        }
        topNavBar.addView(statusBar, 1);
    }

    public void createList()
    {
        solutionsList = findViewById(R.id.listGuessedWords);
        solutionsList.setPadding(10, 10, 10, 10);
        customAdapter = new CustomAdapter(this, guessedWords);
        solutionsList.setAdapter(customAdapter);
        solutionsList.setBackground(ContextCompat.getDrawable(MainPage.this, R.drawable.button_border_grey));
    }
    public void newGameClicked(View v)
    {
        createLetters();
        LU.refreshPossibleSolutions();
        gameScore = 0;
        score.setText(String.valueOf(gameScore));
        listTitle.setText("Found Words");
        bottomScore.setText(String.valueOf(gameScore));
        guessedWords.clear();
        customAdapter.notifyDataSetChanged();
    }




}
