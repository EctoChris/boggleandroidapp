package com.example.chrisford.boggleproject;

/**
 * Created by chrisford on 11/6/18.
 */

public class Tuple {
    public int row;
    public int column;

    public Tuple (int row, int column)
    {
        this.row = row;
        this.column = column;
    }
}
