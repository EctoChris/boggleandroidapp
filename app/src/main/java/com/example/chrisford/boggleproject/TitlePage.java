package com.example.chrisford.boggleproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class TitlePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_page);
    }

    public void PlayButtonClick(View v)
    {
        //Go to next screen using intent
        Intent nextScreen = new Intent(v.getContext(), MainPage.class);
        startActivity(nextScreen);
    }
}
