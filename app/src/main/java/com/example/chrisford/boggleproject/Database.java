package com.example.chrisford.boggleproject;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Database  {

    List<List<String>> letterLists = new ArrayList<>();
    List<String> filePaths = new ArrayList<>();

    //Context used for accessing assets
    Context context;

    public Database(Context context)
    {
        //Reference Application Context from MainPage.java
        this.context = context;

        for (int i = 0; i < 26; i++)
        {
            int numericalValue = i + 97;
            char charValue = (char)numericalValue;
            String path = charValue + "list.txt";
            filePaths.add(path);
        }

        for (int i = 0; i < 26; i++)
        {
            List<String> str = new ArrayList<>();
            letterLists.add(str);
        }

        for (int i = 0; i < 26; i++)
        {
            letterLists.set(i, readFromFile(filePaths.get(i)));
        }
    }

    public Boolean verifyWord(String guessedWord)
    {
        //first get numerical value for first letter
        int firstLetterNum;
        String lowerCaseGuessedWord = guessedWord.toLowerCase();
        char firstLetter = lowerCaseGuessedWord.charAt(0);
        firstLetterNum = (int)firstLetter - 97;
        return doesWordExist(letterLists.get(firstLetterNum), guessedWord);
    }

    private Boolean doesWordExist(List<String> charList, String guessedWord)
    {
        if (charList.contains(guessedWord.toLowerCase()))
            return true;
        return false;
    }

    private List<String> readFromFile(String filePath)
    {
        List<String> tempList = new ArrayList<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filePath)));
            String line;
            while ((line = reader.readLine()) != null)
            {
               tempList.add(line);
            }
        } catch (IOException e)
        {

        } finally
        {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e)
                {

                }
            }
            return tempList;
        }
    }
}
