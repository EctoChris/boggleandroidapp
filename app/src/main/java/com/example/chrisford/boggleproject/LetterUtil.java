package com.example.chrisford.boggleproject;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import bolts.CancellationToken;
import bolts.Task;
import bolts.TaskCompletionSource;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class LetterUtil {
    //Make one instance of random to ensure more randomized results
    static Random r = new Random();
    int screenWidth;

    Boolean colouringInProgress = false;
    Database db;
    private static List<String> possibleSolutions = new ArrayList<>();

    //Ended up hardcoding the exact letters that were found on Hasbro's letter die, as any formula I came up
    // with to randomize letters ended up with fewer possible solutions each game
    public char[] letters = new char[] {  't', 'w', 'o', 'o', 't', 'a', 'e', 'y', 'l', 'd', 'v', 'r', 'l', 'n', 'h', 'n', 'z', 'r', 'a', 'h', 's', 'p', 'c', 'o', 'a', 's', 'p', 'f', 'k', 'f', 'd', 't', 'y', 't', 's', 'i', 'm', 'u', 'o', 'c', 't', 'i', 'b', 'o', 'a', 'b', 'o', 'j', 'm', 'u', 'h', 'q', 'i', 'n', 'a', 'g', 'a', 'e', 'n', 'e', 'w', 'r', 'e', 't', 'v', 'h', 'e', 'g', 'n', 'w', 'e', 'h', 't', 'y', 'e', 'l', 'r', 't', 'd', 'r', 'x', 'i', 'l', 'e', 's', 't', 'o', 'e', 's', 'i', 'i', 'u', 'n', 'e', 'e', 's' };

    public LetterUtil(Database db, int screenWidth)
    {
        this.db = db;
        this.screenWidth = screenWidth;
    }


    //Getter for Colouring Boolean
    public Boolean getColouringInProgress()
    {
        return colouringInProgress;
    }

    //Method to reset Colouring Boolean
    public void resetColouringInProgress()
    {
        colouringInProgress = false;
    }
    //Method to randomize letter using letter array
    public char RandomizeLetter()
    {
        int randomLetter;
        char letter;
        randomLetter = r.nextInt(95);
        letter = letters[randomLetter];
        return letter;
    }

    public String getRandomLetter()
    {
        String returnString="";
        char tempChar = RandomizeLetter();
        if (tempChar == 'q')
            returnString = "Qu";
        else
            returnString +=tempChar;

        return returnString;
    }

    //Getter for possibleSolutionsList
    public List<String> returnPossibleSolutions()
    {
        return possibleSolutions;
    }

    //Method to reset possibleSolutions for new game
    public void refreshPossibleSolutions()
    {
        possibleSolutions.clear();
    }

    //Method to see if currently selected button is next to previously selected
    public Boolean isWithinRangeOfPreviouslyClicked(int currentBtnId, int previousBtnId, ArrayList<Integer> usedIdList)
    {
        //Perform Id Calculations
        if ((previousBtnId + 10) == currentBtnId && !usedIdList.contains(currentBtnId))
             return true;
        else if ((previousBtnId - 10) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId + 1) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId - 1) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId + 9) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId - 9) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId - 11) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;
        else if ((previousBtnId + 11) == currentBtnId && !usedIdList.contains(currentBtnId))
            return true;

        return false;
    }

    public Task<Integer> changeListColour(final CancellationToken ct, final List<Button> usedButtons, final int wordResult)
    {
        //CancellationToken = new CancellationToken();
        // Create a new Task
        final TaskCompletionSource<Integer> tcs = new TaskCompletionSource<>();
        colouringInProgress = true;

        new Thread() {
            @Override
            public void run() {

                // Check if cancelled at start
                if (ct.isCancellationRequested()) {
                    for(Button b : usedButtons)
                    {
                        b.setBackgroundResource(R.drawable.boggle_button);
                    }
                    tcs.setCancelled();
                    return;
                }

                for(Button b : usedButtons)
                {
                    switch(wordResult)
                    {
                        case 0:
                            if (screenWidth < 1100)
                                 b.setBackgroundResource(R.drawable.boggle_button_success);
                            else
                                b.setBackgroundResource(R.drawable.boggle_button_success_tablet);
                            break;
                        case 1:
                            if (screenWidth < 1100)
                                b.setBackgroundResource(R.drawable.boggle_button_alreadyexists);
                            else
                                b.setBackgroundResource(R.drawable.boggle_button_alreadyexists_tablet);
                            break;
                        case 2:
                            if (screenWidth < 1100)
                                b.setBackgroundResource(R.drawable.boggle_button_error);
                            else
                                b.setBackgroundResource(R.drawable.boggle_button_error_tablet);
                            break;
                    }
                }

                //Wait with Cancellation Token ready to be cancelled
                try
                {
                     delayWithCancellationToken(ct, 2000, usedButtons);
                     colouringInProgress = false;
                } catch(InterruptedException ex)
                {
                    for(Button b : usedButtons)
                    {
                        b.setBackgroundResource(R.drawable.boggle_button);
                    }
                    colouringInProgress = false;
                }


                int result = 0;
                while (result < 100) {
                    // Poll isCancellationRequested in a loop
                    if (ct.isCancellationRequested()) {

                        tcs.setCancelled();
                        return;
                    }
                    result++;


                }
                tcs.setResult(result);
            }
        }.start();

        return tcs.getTask();
    }


    public Task<Integer> changeStatusBar(final CancellationToken ct, final TextView statusBar, final int wordResult, final String message)
    {
        // Create a new Task
        final TaskCompletionSource<Integer> tcs = new TaskCompletionSource<>();
        new Thread() {
            @Override
            public void run() {

                // Check if cancelled at start
                if (ct.isCancellationRequested()) {
                    statusBar.setVisibility(INVISIBLE);
                    tcs.setCancelled();
                    return;
                }

                //switch with result, change colour here
                switch(wordResult)
                {
                    case 0:
                        if (ct.isCancellationRequested()) {
                            statusBar.setVisibility(INVISIBLE);
                            tcs.setCancelled();
                            return;
                        }
                        statusBar.setTextColor(Color.GREEN);
                        statusBar.setText(message);
                        break;

                    case 2:
                        if (ct.isCancellationRequested()) {
                            statusBar.setVisibility(INVISIBLE);
                            tcs.setCancelled();
                            return;
                        }
                        statusBar.setTextColor(Color.RED);
                        statusBar.setText(message);
                        break;
                    default:
                        break;
                }
                //Wait with Cancellation Token ready to be cancelled
                try
                {
                    delayWithCancellationToken(ct, 2000, statusBar);
                    colouringInProgress = false;

                } catch(InterruptedException ex)
                {
                    statusBar.setVisibility(INVISIBLE);
                    colouringInProgress = false;
                }


                int result = 0;
                while (result < 100) {
                    // Poll isCancellationRequested in a loop
                    if (ct.isCancellationRequested()) {

                        tcs.setCancelled();
                        return;
                    }
                    result++;
                }
                tcs.setResult(result);
            }
        }.start();

        return tcs.getTask();
    }



    //Task.Delay Implementation with CancellationToken to cancel if new click
    public void delayWithCancellationToken(CancellationToken ct, int delay, List<Button> usedButtons) throws InterruptedException {
        final Task<Void> delayed = Task.delay(delay, ct);
        delayed.waitForCompletion();
        for (Button b : usedButtons)
        {
            b.setBackgroundResource(R.drawable.boggle_button);
        }
    }

  //  Task.Delay overloaded method for statusBar
    public void delayWithCancellationToken(CancellationToken ct, int delay, TextView statusBar) throws InterruptedException {
        final Task<Void> delayed = Task.delay(delay,ct);
        delayed.waitForCompletion();
        statusBar.setVisibility(INVISIBLE);

    }

    public void startFindingCombinations(String[][] letterMatrix, int row, int column)
    {
        //Save the string of starting element
        String tempString;
        String initialString = letterMatrix[row][column];

        //Find surrounding elements in 2 dimensional array (letterMatrix)
        List<Tuple> lettersToCheck = new ArrayList<>(findSurroundingAvailableLetters(row, column));

            /*  Loop through elements that surround current element. Add strings + put
             *  both current and added element in lettersUsed List for calling next method (findThreeLetterWords)
            */

        for(Tuple t : lettersToCheck)
        {
            if(t.row == row && t.column == column)
                continue;
            tempString = initialString + letterMatrix[t.row][t.column];
            List<Tuple> lettersUsed = new ArrayList<>();
            Tuple newTuple = new Tuple(row,column);
            lettersUsed.add(t);
            lettersUsed.add(newTuple);
            findThreeLetterWords(tempString, lettersUsed, t.row, t.column, letterMatrix);
        }
    }

    public void findThreeLetterWords(String previousString, List<Tuple> lettersUsed, int row, int column, String[][] letterMatrix)
    {
        List<Tuple> lettersToCheck = new ArrayList<>(findSurroundingAvailableLetters(row, column));
        List<Tuple> newLettersToCheck = removeLettersUsedFromList(lettersToCheck, lettersUsed);

        for(Tuple t : newLettersToCheck)
        {
            Boolean alreadyInList = false;
            for (Tuple t2 : lettersUsed)
            {
                if (t.row == t2.row && t.column == t2.column)
                    alreadyInList = true;
            }
            if (alreadyInList)
                continue;
            String stringOfThree = previousString + letterMatrix[t.row][t.column];
            if (db.verifyWord(stringOfThree))
            {
                if (!possibleSolutions.contains(stringOfThree))
                    possibleSolutions.add(stringOfThree);
            }
            lettersUsed.add(t);
            find4LetterWords(stringOfThree, lettersUsed, t.row, t.column, letterMatrix);
            lettersUsed.remove(t);
        }
    }

    public void find4LetterWords(String threeLetterString, List<Tuple> lettersUsed, int row, int column, String[][] letterMatrix)
    {
        List<Tuple> lettersToCheck = new ArrayList<>(findSurroundingAvailableLetters(row, column));
        List<Tuple> newLettersToCheck = removeLettersUsedFromList(lettersToCheck, lettersUsed);

        for(Tuple t : newLettersToCheck)
        {
            Boolean alreadyInList = false;
            for (Tuple t2 : lettersUsed)
            {
                if (t.row == t2.row && t.column == t2.column)
                    alreadyInList = true;
            }
            if (alreadyInList)
                continue;
            String stringOfFour = threeLetterString + letterMatrix[t.row][t.column];
            if (db.verifyWord(stringOfFour))
            {
                if (!possibleSolutions.contains(stringOfFour))
                    possibleSolutions.add(stringOfFour);

            }

            List<Tuple> tempLettersUsed = lettersUsed;
            tempLettersUsed.add(t);
            find5LetterWords(stringOfFour, tempLettersUsed, t.row, t.column, letterMatrix);
            tempLettersUsed.remove(t);
        }
    }
    public void find5LetterWords(String fourLetterString, List<Tuple> lettersUsed, int row, int column, String[][] letterMatrix)
    {
        List<Tuple> lettersToCheck = findSurroundingAvailableLetters(row, column);
        List<Tuple> newLettersToCheck = removeLettersUsedFromList(lettersToCheck, lettersUsed);

        for (Tuple t : newLettersToCheck)
        {
            Boolean alreadyInList = false;
            for (Tuple t2 : lettersUsed)
            {
                if (t.row == t2.row && t.column == t2.column)
                alreadyInList = true;
            }
            if (alreadyInList)
                continue;
            String stringOfFive = fourLetterString + letterMatrix[t.row][t.column];
            if (db.verifyWord(stringOfFive))
            {
                if(!possibleSolutions.contains(stringOfFive))
                {
                    possibleSolutions.add(stringOfFive);
                }
            }

            List<Tuple> tempLettersUsed = lettersUsed;
            tempLettersUsed.add(t);
            find6LetterWords(stringOfFive, tempLettersUsed, t.row, t.column, letterMatrix);
            tempLettersUsed.remove(t);
        }
    }

    public void find6LetterWords(String fiveLetterString, List<Tuple> lettersUsed, int row, int column, String[][] letterMatrix)
    {
        List<Tuple> lettersToCheck = findSurroundingAvailableLetters(row, column);
        List<Tuple> newLettersToCheck = removeLettersUsedFromList(lettersToCheck, lettersUsed);

        for (Tuple t : newLettersToCheck)
        {
            Boolean alreadyInList = false;
            for (Tuple t2 : lettersUsed)
            {
                if (t.row == t2.row && t.column == t2.column)
                    alreadyInList = true;
            }
            if (alreadyInList)
                continue;
            String stringOfSix = fiveLetterString + letterMatrix[t.row][t.column];
            if (db.verifyWord(stringOfSix))
            {
                if(!possibleSolutions.contains(stringOfSix))
                {
                    possibleSolutions.add(stringOfSix);
                }
            }
            List<Tuple> tempLettersUsed = lettersUsed;
            tempLettersUsed.add(t);
            find7LetterWords(stringOfSix, tempLettersUsed, t.row, t.column, letterMatrix);;
            tempLettersUsed.remove(t);
        }
    }

    public void find7LetterWords(String sixLetterString, List<Tuple> lettersUsed, int row, int column, String[][] letterMatrix)
    {
        List<Tuple> lettersToCheck = findSurroundingAvailableLetters(row, column);
        List<Tuple> newLettersToCheck = removeLettersUsedFromList(lettersToCheck, lettersUsed);

        for (Tuple t : newLettersToCheck)
        {
            Boolean alreadyInList = false;
            for (Tuple t2 : lettersUsed)
            {
                if (t.row == t2.row && t.column == t2.column)
                    alreadyInList = true;
            }
            if (alreadyInList)
                continue;
            String stringOfSeven = sixLetterString + letterMatrix[t.row] [t.column];
            if (db.verifyWord(stringOfSeven))
            {
                if (!possibleSolutions.contains(stringOfSeven))
                {
                    possibleSolutions.add(stringOfSeven);
                }
            }
            List<Tuple> tempLettersUsed = lettersUsed;
            tempLettersUsed.add(t);
            tempLettersUsed.remove(t);
        }
    }

    public List<Tuple> findSurroundingAvailableLetters(int row, int column)
    {

        //Create 2 empty lists to be filled
        List<Tuple> tempList1 = new ArrayList<>();
        List<Tuple> tempList2 = new ArrayList<>();

        //Create final list for returning available surrounding letters
        List<Tuple> surroundingAvailableLetters = new ArrayList<>();

        //Find potential surrounding letters given the row the current letter is situated (ignoring columns)
        switch(row)
        {
            case 0:
                for (int i = 0; i < 4; i++)
                {
                    tempList1.add(new Tuple (0, i));
                    tempList1.add(new Tuple (1, i));
                }
                break;
            case 1:
                for (int i = 0; i < 4; i++)
                {
                    tempList1.add(new Tuple (0, i));
                    tempList1.add(new Tuple (1, i));
                    tempList1.add(new Tuple (2, i));
                }
                break;
            case 2:
                for (int i = 0; i < 4; i++)
                {
                    tempList1.add(new Tuple(1, i));
                    tempList1.add(new Tuple(2, i));
                    tempList1.add(new Tuple(3, i));
                }
                break;
            case 3:
                for (int i = 0; i < 4; i++)
                {
                    tempList1.add(new Tuple(2, i));
                    tempList1.add(new Tuple(3, i));
                }
                break;
        }

        //Find potential surrounding letters given the column the current letter is situated (ignoring rows)
        switch (column)
        {
            case 0:
                for (int i = 0; i < 4; i++)
                {
                    tempList2.add(new Tuple (i, 0));
                    tempList2.add(new Tuple (i, 1));
                }
                break;
            case 1:
                for (int i = 0; i < 4; i++)
                {
                    tempList2.add(new Tuple(i, 0));
                    tempList2.add(new Tuple(i, 1));
                    tempList2.add(new Tuple(i, 2));
                }
                break;
            case 2:
                for (int i = 0; i < 4; i++)
                {
                    tempList2.add(new Tuple(i, 1));
                    tempList2.add(new Tuple(i, 2));
                    tempList2.add(new Tuple(i, 3));
                }
                break;
            case 3:
                for (int i = 0; i < 4; i++)
                {
                    tempList2.add(new Tuple(i, 2));
                    tempList2.add(new Tuple(i, 3));
                }
                break;
        }
        //combine both lists into surroundingAvailableLetters where letters match up & it isnt the starting letter
        for(Tuple t : tempList1)
        {
            for (Tuple t2 : tempList2)
            {
                if (t2.row == row && t2.column ==column)
                    continue;
                if (t2.row == t.row && t2.column == t.column)
                        surroundingAvailableLetters.add(new Tuple(t.row, t.column));
            }
        }
        return surroundingAvailableLetters;
    }

    public List<Tuple> removeLettersUsedFromList(List<Tuple> potentialLettersToCheck, List<Tuple> lettersUsed)
    {
        List<Tuple> newLettersToCheck = new ArrayList<>();
        for (Tuple t : potentialLettersToCheck)
        {
            if (!lettersUsed.contains(t))
                newLettersToCheck.add(t);
        }
        return newLettersToCheck;
    }
}
